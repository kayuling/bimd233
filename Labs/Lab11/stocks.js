window.onload = function() {
    document.getElementById("button").onclick = function() {
        function stocks(name, cap, sales, profit, employees) {
            this.company = name;
            this.market = cap;
            this.sales = sales;
            this.profit = profit;
            this.number = employees;
            this.data = function() {
                return this.sales + "-" + this.profit;
            }
        };    
         var stocks = [
            new stocks("Microsoft", "$381.7 B", "$86.8 B", "$22.1 B", "128,000"),
            new stocks("Symetra Financial", "$2.7 B", "$2.2 B", "$254.4 M", "1,400"),
            new stocks("Micron Technology", "$37.6 B", "$16.4 B", "$3 B", "30,400"),
            new stocks("F5 Networks", "$9.5 B", "$1.7 B", "$311.2 M", "3,834"),
            new stocks("Expedia", "$10.8 B", "$5.8 B", "$398.1 M", "18,210"),
            new stocks("Nautilus", "$476 M", "$274.4 M", "$18.8 M", 340),
            new stocks("Heritage Financial", "$531 M", "$137.6 M", "$21 M", 748),
            new stocks("Cascade Microtech", "$239 M", "$136 M", "$9.9 M", 449),
            new stocks("Nike", "$83.1 B", "$27.8 B", "$2.7 B", "56,500"),
            new stocks("Alaska Air Group", "$7.9 B", "$5.4 B", "$605 M", "13,952"),
        ];
        var html = "";
        stocks.forEach(function(v){
            html += "<tr><td>" + v["company"] + "</td><td>" + v["market"] + "</td><td>" + v["sales"] + "</td><td>" + v["profit"] + "</td><td>" + v["number"] + "</td></tr>";
        });
        document.getElementById("results").innerHTML = html;
    }

}