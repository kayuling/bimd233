window.onload = function() {
  function average(date, high, low) {
    this.date = date;
    this.high = high;
    this.low = low;
  };

  var average = [
    new average("Friday, May 13", 82, 55),
    new average("Saturday, May 14", 75, 52),
    new average("Sunday, May 15", 69, 52),
    new average("Monday, May 16", 69, 48),
    new average("Tuesday, May 17", 68, 51),
  ];
  var html = "";
  average.forEach(function(v) {
    html += "<tr><td>" + v["date"] + "</td><td>" + v["high"] + "</td><td>" + v["low"] + "</td></tr>";
  });
  document.getElementById("results").innerHTML = html;

  var high = [82, 75, 69, 69, 68];
  var low = [55, 52, 52, 48, 51];

  var sumHigh = high.reduce(function(a, b) {
    return a + b
  });
  var highAvg = sumHigh/high.length;

  var sumLow = low.reduce(function(a, b) {
    return a + b
  });
  var lowAvg = sumLow/low.length;

  var html = "";
  html += "<tr><td>" + highAvg + "</td><td>" + lowAvg + "</td></tr>";
  document.getElementById("results2").innerHTML = html;
}