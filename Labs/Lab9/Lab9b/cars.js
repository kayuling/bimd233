window.onload = function() {
    var html = "";
    var car1 = makeCar("Lamborghini", "Gallardo", 2014, 150604);
    var car2 = makeCar("Subaru", "WRX STI", 2016, 34695);
    var car3 = makeCar("Nissan", "GTR", 2015, 101770.00);
    var car4 = makeCar("Audi", "A4", 2016, 38000);
    var car5 = makeCar("BMW", "i8", 2016, 140700);
    var cars = [car1, car2, car3, car4, car5];
    html += "<tr><td>"+ car1[0] +"</td><td>"+ car1[1] +"</td><td>"+ car1[2] +"</td><td>"+ "$" + car1[3] +"</td></tr>";
    html += "<tr><td>"+ car2[0] +"</td><td>"+ car2[1] +"</td><td>"+ car2[2] +"</td><td>"+ "$" + car2[3] +"</td></tr>";
    html += "<tr><td>"+ car3[0] +"</td><td>"+ car3[1] +"</td><td>"+ car3[2] +"</td><td>"+ "$" + car3[3] + "</td></tr>";
    html += "<tr><td>"+ car4[0] +"</td><td>"+ car4[1] +"</td><td>"+ car4[2] +"</td><td>"+ "$" + car4[3] +"</td></tr>";
    html += "<tr><td>"+ car5[0] +"</td><td>"+ car5[1] +"</td><td>"+ car5[2] +"</td><td>"+ "$" + car5[3] +"</td></tr>";
                       
    document.getElementById("results").innerHTML += html;
    function makeCar(make, model, year, price) {
    var car = [make, model, year, price];
        return car;
}
    }