window.onload = function() {
    var radii = [10,20,30]; 
    var html = "";
    var geometry = calcCircleGeometries(radii[0]);
    html += "<tr><td>"+ geometry[0] +"</td><td>"+ geometry[1] + "</td><td>"+ geometry[2] + "</td><tr>"
    var geometry2 = calcCircleGeometries(radii[1]);
    html += "<tr><td>"+ geometry2[0] +"</td><td>"+ geometry2[1] +"</td><td>"+ geometry2[2]+ "</td></tr>";
    var geometry3=calcCircleGeometries(radii[2]);
    html+="<tr><td>"+ geometry3[0] +"</td><td>"+ geometry3[1] +"</td><td>"+ geometry3[2] +"</td></tr>";
    document.getElementById("results").innerHTML += html;
    function calcCircleGeometries(radius){
            const pi = Math.PI;
            var area = pi*radius*radius;
            var circumference = 2*pi*radius;
            var diameter = 2*radius;
            var geometries = [area, circumference, diameter];
            return geometries
    
        }
}