$('li').on("mouseover", function() {
  $(this).attr('id', 'selected');
  $(this).text('Focused!');
});

$('li').on("mouseleave", function() {
  $(this).attr('id', 'deselected');
  $(this).text('Not focused');
});