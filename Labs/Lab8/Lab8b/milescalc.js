window.onload = function() {
    document.getElementById("submit").onclick = function() {
        var vehiclename = document.getElementById("car").value;
        var date = document.getElementById("date").value;
        var miles = document.getElementById("miles").value;
        var totalmiles = miles * .59;
        
        document.getElementById("results").innerHTML = "Car Name; " + vehiclename + "<br>Date: " + date + "<br>Miles: " + miles + "<br>Deduction: $" + totalmiles;
        return false;
    }
}