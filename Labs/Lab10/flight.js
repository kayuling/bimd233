window.onload = function() {
  function Flight(airline, number, origin, destination, dep_time, arrival_time, arrival_gate) {
    this.airline = airline;
    this.number = number;
    this.origin = origin;
    this.destination = destination;
    this.dep_time = dep_time;
    this.arrival_time = arrival_time;
    this.arrival_gate = arrival_gate;
    this.flightDuration = function() {
      return this.dep_time + "-" + this.arrival_time;
    }
  };
  var alaskaFlight = new Flight('Alaska', 'AK333', 'B737', 'KSEA', 'May 2, 2016 6:00 PM', 'May 3, 2016 7:20 PM', 'C7');

  var unitedFlight = new Flight('United', 'U2001', 'A320', 'KORD', 'May 5, 2016 7:15 AM', 'May 6, 2016 9:20 AM', 'N17');

  var deltaFlight = new Flight('Delta', 'DAL', 'MD90', 'KORD', 'May 3, 2016 10:30 AM', 'May 4, 2016 11:50 AM', 'L9');

   var evaFlight = new Flight('Eva', 'AK569', 'C737', 'KLA', 'May 3, 2016 4:00 PM', 'May 4, 2016 5:30 PM', 'R7');
  
   var anaFlight = new Flight('ANA', 'AR834', 'B963', 'KNYC', 'May 4, 2016 5:45 PM', 'May 5, 2016 6:50 PM', 'D8');
  
  
  var html = '';
  html += "<tr><td>"+alaskaFlight["airline"]+"</td><td>"+alaskaFlight["number"]+"</td><td>"+alaskaFlight["origin"]+"</td><td>"+alaskaFlight["destination"]+"</td><td>"+alaskaFlight["dep_time"]+"</td><td>"+alaskaFlight["arrival_time"]+"</td><td>"+alaskaFlight["arrival_gate"]+"</td></tr>";

 html += "<tr><td>"+unitedFlight["airline"]+"</td><td>"+unitedFlight["number"]+"</td><td>"+unitedFlight["origin"]+"</td><td>"+unitedFlight["destination"]+"</td><td>"+unitedFlight["dep_time"]+"</td><td>"+unitedFlight["arrival_time"]+"</td><td>"+unitedFlight["arrival_gate"]+"</td></tr>";
  
 html += "<tr><td>"+deltaFlight["airline"]+"</td><td>"+deltaFlight["number"]+"</td><td>"+deltaFlight["origin"]+"</td><td>"+deltaFlight["destination"]+"</td><td>"+deltaFlight["dep_time"]+"</td><td>"+deltaFlight["arrival_time"]+"</td><td>"+deltaFlight["arrival_gate"]+"</td></tr>";
  
 html += "<tr><td>"+evaFlight["airline"]+"</td><td>"+evaFlight["number"]+"</td><td>"+evaFlight["origin"]+"</td><td>"+evaFlight["destination"]+"</td><td>"+evaFlight["dep_time"]+"</td><td>"+evaFlight["arrival_time"]+"</td><td>"+evaFlight["arrival_gate"]+"</td></tr>";

 html += "<tr><td>"+anaFlight["airline"]+"</td><td>"+anaFlight["number"]+"</td><td>"+anaFlight["origin"]+"</td><td>"+anaFlight["destination"]+"</td><td>"+anaFlight["dep_time"]+"</td><td>"+anaFlight["arrival_time"]+"</td><td>"+anaFlight["arrival_gate"]+"</td></tr>";
document.getElementById("table_data").innerHTML=html;
}